/**
 * Copyright (c) 2011-2016, Eason Pan(pylxyhome@vip.qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.weixin.song.interceptor;

import javax.servlet.http.HttpServletRequest;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.kit.PropKit;

/**
 * 微信访问拦截器
 */
public class WeixinAccessInterceptor implements Interceptor {
	public static final String NOT_WEIXIN_PAGE="/WEB-INF/view/wx/not_weixin_access.jsp";
	public void intercept(Invocation inv) {
		if(!isWeixin(inv.getController().getRequest())){
			inv.getController().renderJsp(NOT_WEIXIN_PAGE);
		}else{
			inv.invoke();
		}
	}
	private boolean isWeixin(HttpServletRequest request){
		String ua = request.getHeader("user-agent")
		          .toLowerCase();
		return ua.indexOf("micromessenger") > 0;
	}
}
