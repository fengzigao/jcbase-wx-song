package cn.weixin.song.controller.wx;

import java.util.List;

import cn.weixin.song.interceptor.WeixinAccessInterceptor;
import cn.weixin.song.model.Activity;
import cn.weixin.song.model.ActivityAward;
import cn.weixin.song.model.ActivityShareRecord;
import cn.weixin.song.model.App;
import cn.weixin.song.model.User;
import cn.weixin.song.util.WeixinUtils;

import com.github.sd4324530.fastweixin.api.response.GetSignatureResponse;
import com.jcbase.core.controller.JCBaseController;
import com.jcbase.core.view.InvokeResult;
import com.jfinal.aop.Before;
import com.jfinal.kit.JsonKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.tx.Tx;

/**
 * 幸运转盘
 * @author eason
 */
public class WxLuckController extends JCBaseController {
    
	@Before(WeixinAccessInterceptor.class)
	public void index() {
		Integer aid=this.getParaToInt("aid");
		if(aid!=null){
			String openid=this.getPara("openid");
			if(StrKit.isBlank(openid)){
				openid=this.getCookie("openid");
			}
			this.setAttr("openid",openid);
			this.setCookie("openid", openid, 60*60*24*30);
			Activity activity=Activity.dao.findById(aid);
			if(activity!=null){
		        App app=App.dao.findById(activity.getAppId());
		        this.setAttr("wxAppId",app.getWxAppId());
		        this.setAttr("app",app); 
				GetSignatureResponse  getSignatureResponse =WeixinUtils.getJsAPI(app.getWid()).getSignature(this.getThisUrl());
				this.setAttr("appData", getSignatureResponse); 
				List<ActivityAward> awardlist=ActivityAward.dao.getActivityAwardList(aid);
				String[] awardNames=new String[awardlist.size()];
				String[] colors=new String[awardlist.size()];
				for(int i=0;i<awardlist.size();i++){
					ActivityAward item =awardlist.get(i);
					awardNames[i]=item.getName();
					colors[i]=item.getBgColor();
				}
				this.setAttr("awardNames", JsonKit.toJson(awardNames));
				this.setAttr("colors", JsonKit.toJson(colors));
				this.setAttr("activity",activity);
				this.setAttr("openid", openid);
				this.render("luck_index.jsp");
				return;
			}
		}
		this.renderError(404);
    }
	
	/**
	 * 抽奖接口
	 */
	@Before(Tx.class)
	public void lottery(){ 
		String openid=this.getPara("openid");
		Integer aid=this.getParaToInt("aid");
		InvokeResult result=Activity.dao.lottery(openid,aid);
		this.renderJson(result);
	}

	/**
	 * 提交用户信息
	 * @author eason
	 */
	@Before(Tx.class)
	public void submitInfo(){
		String openid=this.getPara("openid"); 
		Integer appId=this.getParaToInt("appId"); 
		String mobilephone=this.getPara("mobilephone");
		InvokeResult result=User.dao.updateUserInfo(appId,openid,mobilephone);
		this.renderJson(result);
	}
	
	/**
	 * 分享记录
	 */
	@Before(Tx.class)
	public void share(){ 
		String openid=this.getPara("openid");
		Integer type=this.getParaToInt("type");
		Integer aid=this.getParaToInt("aid");
		if(StrKit.isBlank(openid)||type==null||aid==null){
			this.renderJson( InvokeResult.failure(""));
			return;
		}
		Activity activity=Activity.dao.findById(aid);
		InvokeResult result=ActivityShareRecord.dao.addActivityShareRecord(activity,openid,type);
		this.renderJson(result);
	}
}
