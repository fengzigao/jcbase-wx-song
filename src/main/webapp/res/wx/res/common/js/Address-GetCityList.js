function SaveCityIdAndCode(n, t, i) {
   location.href = $("#hidReturnUrl").val()+"?cityid="+n;
}
function getIndexBeginLocation() {
    if (navigator.userAgent.indexOf("Android") > -1 && navigator.userAgent.indexOf("baiduboxapp") > -1) {
        clouda.lightapp("QzvF52xRZefgVvhiBVQP1epE",
        function() {
            clouda.device.geolocation.get({
                onsuccess: onIndexBeginSuccess,
                onfail: onIndexBeginFail
            })
        });
        return
    }
    if (navigator.userAgent.indexOf("Android") > -1 && navigator.userAgent.indexOf("HybridETS") > -1) {
        window.geolocation.getCurrentPosition("onIndexBeginSuccess", "onIndexBeginFail");
        return
    }
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showIndexBeginPosition, showIndexBeginError, geooptions);
        return
    }
}
function showIndexBeginPosition(n) {
    var t = n.coords.latitude,
    i = n.coords.longitude;
    window.BMap.Convertor.translate(new window.BMap.Point(i, t), 0, getIndexBeginLocationCity)
}
function showIndexBeginError() {}
function getIndexBeginLocationCity(n) {
    $.ajax({
        url: $("#hidGetLocationCityUrl").val(),
        global: !1,
        type: "POST",
        dataType: "json",
        data: {
            userLat: n.lat,
            userLong: n.lng
        },
        success: function(n) {
            if (n.StatusCode != 200) {
                PopUp(n.Message);
                return
            }
            $(".select").text(n.CityName)
        },
        error: function() {}
    })
}
function getIndexLocation() {
    if ($("#userCity").text("定位中..."), navigator.userAgent.indexOf("Android") > -1 && navigator.userAgent.indexOf("baiduboxapp") > -1 && flag) {
        clouda.device.geolocation.get({
            onsuccess: onIndexSuccess,
            onfail: onIndexFail
        });
        return
    }
    if (navigator.userAgent.indexOf("Android") > -1 && navigator.userAgent.indexOf("HybridETS") > -1) {
        window.geolocation.getCurrentPosition("onIndexSuccess", "onIndexFail");
        return
    }
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showIndexPosition, showIndexError, geooptions);
        return
    }
    $("#userCity").text("定位失败，无法获取城市")
}
function showIndexPosition(n) {
    var t = n.coords.latitude,
    i = n.coords.longitude;
    window.BMap.Convertor.translate(new window.BMap.Point(i, t), 0, getIndexLocationCity)
}
function showIndexError() {
    $("#userCity").text("定位失败，无法获取城市")
}
function getIndexLocationCity(n) {
    $.ajax({
        url: $("#hidGetLocationCityUrl").val(),
        type: "POST",
        dataType: "json",
        data: {
            userLat: n.lat,
            userLong: n.lng
        },
        success: function(n) {
            if (n.StatusCode != 200) {
                PopUp(n.Message);
                return
            }
            $("#userCity").text(n.Result.CityName.replace("市", "")),
            $("#userCity").attr("value", n.Result.CityId),
            $("#userCity").attr("code", n.Result.CityCode)
        },
        error: function() {
            $("#userCity").text("定位失败，无法获取城市")
        }
    })
}
function getSupplierListLocation() {
    if ($("#address").text("定位中..."), navigator.userAgent.indexOf("Android") > -1 && navigator.userAgent.indexOf("baiduboxapp") > -1 && flag) {
        clouda.device.geolocation.get({
            onsuccess: onAdressSuccess,
            onfail: onAdressFail
        });
        return
    }
    if (navigator.userAgent.indexOf("Android") > -1 && navigator.userAgent.indexOf("HybridETS") > -1) {
        window.geolocation.getCurrentPosition("onAdressSuccess", "onAdressFail");
        return
    }
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showSupplierListPosition, showSupplierListError, geooptions);
        return
    }
    $("#address").text("定位失败，无法获取城市")
}
function showSupplierListPosition(n) {
    var t = n.coords.latitude,
    i = n.coords.longitude;
    window.BMap.Convertor.translate(new window.BMap.Point(i, t), 0, getSupplierListLocationCity)
}
function showSupplierListError() {
    $(".shuaxin img").removeClass("shuaxinP"),
    $("#address").text("定位失败，无法获取城市")
}
function getSupplierListLocationCity(n) {
    var t = $("#hidSupplierGroupId").val();
    $.ajax({
        url: $("#hidGetLocationCityUrl").val(),
        type: "POST",
        dataType: "json",
        data: {
            userLat: n.lat,
            userLong: n.lng
        },
        success: function(n) {
            if ($(".shuaxin img").removeClass("shuaxinP"), n.StatusCode != 200) {
                PopUp(n.Message);
                return
            }
            $("#address").text(n.Result.Address),
            updateSupplierList(t)
        },
        error: function() {
            $(".shuaxin img").removeClass("shuaxinP"),
            $("#address").text("定位失败，无法获取城市")
        }
    })
}
function updateSupplierList(n) {
    var t = 1;
    $.ajax({
        url: $("#hidBranchSupplierListUrl").val(),
        type: "POST",
        dataType: "html",
        data: {
            supplierGroupId: n,
            featureId: t
        },
        success: function(n) {
            $("#branchSuppliers").html(n),
            bindSupplierDetailEvent()
        },
        error: function() {}
    })
}
function getShoppingListLocation() {
    if ($("#address").text("定位中..."), navigator.userAgent.indexOf("Android") > -1 && navigator.userAgent.indexOf("baiduboxapp") > -1 && flag) {
        clouda.device.geolocation.get({
            onsuccess: onSupplierSuccess,
            onfail: onSupplierFail
        });
        return
    }
    if (navigator.userAgent.indexOf("Android") > -1 && navigator.userAgent.indexOf("HybridETS") > -1) {
        window.geolocation.getCurrentPosition("onSupplierSuccess", "onSupplierFail");
        return
    }
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showShoppingListPosition, showShoppingListError, geooptions);
        return
    }
    $("#address").text("定位失败，无法获取城市")
}
function showShoppingListPosition(n) {
    var t = n.coords.latitude,
    i = n.coords.longitude;
    window.BMap.Convertor.translate(new window.BMap.Point(i, t), 0, getShoppingListLocationCity)
}
function showShoppingListError() {
    $(".shuaxin img").removeClass("shuaxinP"),
    $("#address").text("定位失败，无法获取城市")
}
function getShoppingListLocationCity(n) {
    $.ajax({
        url: $("#hidGetLocationCityUrl").val(),
        type: "POST",
        dataType: "json",
        data: {
            userLat: n.lat,
            userLong: n.lng
        },
        success: function(n) {
            if ($(".shuaxin img").removeClass("shuaxinP"), n.StatusCode != 200) {
                PopUp(n.Message);
                return
            }
            $("#address").text(n.Result.Address),
            getSupplierList($("#cuisien").find(".xz").attr("id"), $("#reginlist").find(".xz").attr("childid"), $("#hidCityId").val(), $("#delivery").find(".xz").val())
        },
        error: function() {
            $(".shuaxin img").removeClass("shuaxinP"),
            $("#address").text("定位失败，无法获取城市")
        }
    })
}
var geooptions, flag, onIndexBeginSuccess, onIndexBeginFail, onIndexSuccess, onIndexFail, onAdressSuccess, onAdressFail, onSupplierSuccess, onSupplierFail;
$(document).ready(function() {
    tapcolor(".clkitm"),
    $(".las").click(function() {
        var n, t;
        if ($(this).attr("code") == "") {
            getIndexLocation();
            return
        }
        n = $(".prer03").find(".city-select").length,
        n > 0 && $(".prer03").find(".city-select").toggleClass("city-select"),
        t = $(".prer03").find(".city-select").find("p[value=" + $(this).attr("value") + "]").length,
        t > 0 && $(".prer03").find(".city-select").find("p[value=" + $(this).attr("value") + "]").addClass("sel"),
        SaveCityIdAndCode($(this).attr("value"), $(this).attr("code"), $(this).text())
    }),
    $(".prer03 >li p[name!=fir]").click(function() {
        var r = $(".prer03").find(".city-select").length,
        n,
        t,
        i;
        r > 0 && $(".prer03").find(".city-select").toggleClass("city-select"),
        n = $(this).attr("class"),
        n || $(this).addClass("city-select"),
        t = $(this).attr("code"),
        i = $(this).attr("value"),
        SaveCityIdAndCode(i, t, $(this).text())
    })
}),
geooptions = {
    enableHighAccuracy: !0,
    maximumAge: 5e3,
    timeout: 5e3
},
flag = !1,
clouda.lightapp("QzvF52xRZefgVvhiBVQP1epE",
function() {
    flag = !0
}),
onIndexBeginSuccess = function(n) {
    var t = n.longitude,
    i = n.latitude,
    r = new BMap.Point(t, i);
    getIndexBeginLocationCity(r)
},
onIndexBeginFail = function() {},
onIndexSuccess = function(n) {
    var i = n.longitude,
    r = n.latitude,
    t;
    $("#userCity").text("地址解析中..."),
    t = new BMap.Point(i, r),
    getIndexLocationCity(t)
},
onIndexFail = function() {
    $("#userCity").text("定位失败，无法获取城市")
},
onAdressSuccess = function(n) {
    var i = n.longitude,
    r = n.latitude,
    t;
    $("#address").text("地址解析中..."),
    t = new BMap.Point(i, r),
    getSupplierListLocationCity(t)
},
onAdressFail = function() {
    $("#address").text("定位失败，无法获取城市")
},
onSupplierSuccess = function(n) {
    var i = n.longitude,
    r = n.latitude,
    t;
    $("#address").text("地址解析中..."),
    t = new BMap.Point(i, r),
    getShoppingListLocationCity(t)
},
onSupplierFail = function() {
    $("#address").text("定位失败，无法获取城市")
}